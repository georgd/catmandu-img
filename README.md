# Docker image catmandu-img
Creates a docker image that bundles everything that is required for offering perl modules Catmandu and Catmandu::MARC.

* Based on [Debian bookworm image](https://hub.docker.com/_/debian).
* _Abstract_ image: doesn't provide any services but has to be derived by another docker image.

## Build image
* Change to the directory containing `Dockerfile`:  
  `docker build  -f ./Dockerfile -t catmandu-img .`


# Minimal Dockerfile of a base image with Catmandu core on Debian stretch
FROM debian:bookworm

MAINTAINER Georg Mayr-Duffner "georg.mayr-duffner@wu.ac.at"

ADD apt.txt .
# Perl packages used by Catmandu (if available as Debian package) and cpanm
RUN apt-get update && apt-get install -y --no-install-recommends \
  $(grep -vE "^\s*#" apt.txt | tr "\n" " ") cpanminus 

# install from source
RUN cpanm MARC::File::XML -f
RUN cpanm Catmandu Catmandu::MARC
